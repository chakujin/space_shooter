﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diagonal : Weapon
{
    public GameObject laserBullet;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {

        Instantiate(laserBullet, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation, null);
        Instantiate(laserBullet, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation, null);
        Instantiate(laserBullet, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation, null);

    }
}
