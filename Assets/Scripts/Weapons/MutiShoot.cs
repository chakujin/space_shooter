﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutiShoot : Weapon
{
    public GameObject laserBullet;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate(laserBullet, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity, null);
        Instantiate(laserBullet, this.transform.position, Quaternion.identity, null);
    }
}
