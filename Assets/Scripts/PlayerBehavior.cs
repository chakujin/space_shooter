﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour
{
    private Vector2 axis;
    public float speed;
    public Vector2 limits;
    public Propeler prop;
    public Weapon weapon;
    public float shootTime=0f;
    public GameObject graficos;
    public ScoreManager scoreManager;
    public AudioSource audioSource;
    public ParticleSystem ps;
    public int lives = 3;
    private bool iamDead = false;
    [SerializeField] Collider2D mycollider;
    void Update()
    {
        if(iamDead){
            return;
        }
        shootTime += Time.deltaTime;
        transform.Translate(axis * speed * Time.deltaTime);
     
        if (transform.position.x > limits.x)
        {
            transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
        }
        else if (transform.position.x < -limits.x)
        {
            transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
        }

        if (transform.position.y > limits.y)
        {
            transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
        }
        else if (transform.position.y < -limits.y)
        {
            transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
        }

        if (axis.x > 0)
        {
            prop.BlueFire();
        }
        else
        {
            prop.Stop();
        }


        if (axis.x < 0)
        {
            prop.RedFire();
        }
        else
        {
            prop.Stop1();
        }

    }
    public void SetAxis(Vector2 currentAxis)
    {
        axis = currentAxis;

    }

    public void Shoot(){
        
       if (!iamDead && shootTime>weapon.GetCadencia()){
        shootTime = 0;
        weapon.Shoot();
       }
    }

    public void OnTriggerEnter2D(Collider2D other) {
        if (other.tag  == "Meteor"){
            StartCoroutine(DestroyShip());
        }
    }

        IEnumerator DestroyShip(){
        //Indico que estoy muerto
        iamDead=true;

        //Me quito una vida
        lives--;
       
        //Desactivo el grafico
        graficos.SetActive(false);

        mycollider.enabled = false;

        ps.Play();

        //Lanzo sonido de explosion
       
        audioSource.Play();
      
        prop.gameObject.SetActive(false);

        scoreManager.LoseLife();

        yield return new WaitForSeconds(1.0f);

        if(lives>0){
            StartCoroutine(InMortal());
        }
    }



    IEnumerator InMortal(){
        iamDead = false;
        graficos.SetActive(true);
        
        //Activo el propeller
        prop.gameObject.SetActive(true);

        for(int i=0; i<15; i++){
            graficos.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            graficos.SetActive(true);
                yield return new WaitForSeconds(0.1f);
        }

        mycollider.enabled = true;
    }



}