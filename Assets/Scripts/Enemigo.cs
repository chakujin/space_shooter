﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{

    private Vector2 speed = new Vector2(0, 0);
    public GameObject bullet;
    private void Start()
    {
        StartCoroutine(EnemyBehaviour());
    }

    void Update()
    {
         transform.Translate(speed * Time.deltaTime);
    }

    IEnumerator EnemyBehaviour()
    {
        while (true)
        {
            //Avanza horizontalmente
            speed.x = -3f;
           // speed.y = -3f;
            yield return new WaitForSeconds(1.0f);
            speed.x = 0f;
           // speed.y = 0f;
            yield return new WaitForSeconds(1.0f);
           // speed.x = -3f;
            //speed.y = 3f;
           // yield return new WaitForSeconds(1.0f);
           // speed.x = 0f;
           // speed.y = 0f;
           // yield return new WaitForSeconds(1.0f);

            //Dispara
            
           GameObject bala = Instantiate(bullet, transform.position, Quaternion.identity,null);
            bala.transform.Rotate(0,0,-180);
        }
    }
}
